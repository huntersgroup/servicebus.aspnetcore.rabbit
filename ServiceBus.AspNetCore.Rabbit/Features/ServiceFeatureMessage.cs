﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;

namespace ServiceBus.AspNetCore.Features
{
    /// <summary>
    /// 
    /// </summary>
    public class ServiceFeatureMessage : IFeatureCollection, IHttpRequestFeature, IHttpResponseFeature, IHttpConnectionFeature, IDisposable
    {
        private const string DefaultSchema = "http";
        private IHttpRequestFeature currentHttpRequestFeature;
        private IHttpResponseFeature currentHttpResponseFeature;
        private IHttpConnectionFeature currentHttpConnectionFeature;
        private List<KeyValuePair<Type, object>> otherFeatures;

        private string scheme;
        private string pathBase;
        private string path;
        private string queryString;

        private readonly object onStartingSync = new object();
        private readonly object onCompletedSync = new object();
        private volatile bool disposed;

        /// <summary>
        /// 
        /// </summary>
        public ServiceFeatureMessage()
        {
            this.currentHttpRequestFeature = this;
            this.currentHttpResponseFeature = this;
            this.currentHttpConnectionFeature = this;

            //Set connection feature properties
            this.currentHttpConnectionFeature.RemoteIpAddress = IPAddress.IPv6Any;
            this.currentHttpConnectionFeature.LocalIpAddress = null;
            this.currentHttpConnectionFeature.ConnectionId = Guid.NewGuid().ToString("D");

            this.OnStartingActions = new List<KeyValuePair<Func<object, Task>, object>>();
            this.OnCompletedActions = new List<KeyValuePair<Func<object, Task>, object>>();
        }

        internal Exception ApplicationException { get; set; }

        internal bool HasApplicationException => this.ApplicationException != null;

        internal List<KeyValuePair<Func<object, Task>, object>> OnStartingActions { get; }

        internal List<KeyValuePair<Func<object, Task>, object>> OnCompletedActions { get; }
        
        #region IFeatureCollection Implementation

        /// <inheritdoc />
        public bool IsReadOnly => false;

        /// <inheritdoc />
        public int Revision { get; private set; }

        /// <inheritdoc />
        public object this[Type key]
        {
            get
            {
                if (key == typeof(IHttpRequestFeature)) { return currentHttpRequestFeature; }
                if (key == typeof(IHttpResponseFeature)) { return currentHttpResponseFeature; }
                if (key == typeof(IHttpConnectionFeature)) { return currentHttpConnectionFeature; }

                if (otherFeatures == null) return null;

                foreach (var kv in otherFeatures)
                {
                    if (kv.Key == key) return kv.Value;
                }

                return null;
            }

            set
            {
                this.Revision++;

                if (key == typeof(IHttpRequestFeature)) { currentHttpRequestFeature = value as dynamic; }
                if (key == typeof(IHttpResponseFeature)) { currentHttpResponseFeature = value as dynamic; }
                if (key == typeof(IHttpConnectionFeature)) { currentHttpConnectionFeature = value as dynamic; }

                if (otherFeatures == null)
                {
                    otherFeatures = new List<KeyValuePair<Type, object>>();
                }

                for (var i = 0; i < otherFeatures.Count; i++)
                {
                    if (otherFeatures[i].Key == key)
                    {
                        otherFeatures[i] = new KeyValuePair<Type, object>(key, value);
                        return;
                    }
                }

                otherFeatures.Add(new KeyValuePair<Type, object>(key, value));
            }
        }

        /// <inheritdoc />
        TFeature IFeatureCollection.Get<TFeature>()
        {
            return (TFeature)this[typeof(TFeature)];
        }

        /// <inheritdoc />
        void IFeatureCollection.Set<TFeature>(TFeature instance)
        {
            this[typeof(TFeature)] = instance;
        }

        /// <inheritdoc />
        IEnumerator<KeyValuePair<Type, object>> IEnumerable<KeyValuePair<Type, object>>.GetEnumerator()
        {
            return GetFeatureCollectionEnumerable().GetEnumerator();
        }

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetFeatureCollectionEnumerable().GetEnumerator();
        }

        IEnumerable<KeyValuePair<Type, object>> GetFeatureCollectionEnumerable()
        {
            if (currentHttpRequestFeature != null)
            {
                yield return new KeyValuePair<Type, object>(typeof(IHttpRequestFeature), currentHttpRequestFeature);
            }

            if (currentHttpResponseFeature != null)
            {
                yield return new KeyValuePair<Type, object>(typeof(IHttpResponseFeature), currentHttpResponseFeature);
            }

            if (this.otherFeatures == null) yield break;

            foreach (var feature in this.otherFeatures)
            {
                yield return feature;
            }
        }

        #endregion

        #region IHttpRequestFeature Implementation

        /// <inheritdoc />
        string IHttpRequestFeature.Protocol
        {
            get; set;
        }

        /// <inheritdoc />
        string IHttpRequestFeature.Scheme
        {
            get => scheme ?? DefaultSchema;
            set => scheme = value;
        }

        /// <inheritdoc />
        string IHttpRequestFeature.Method
        {
            get; set;
        }

        /// <inheritdoc />
        string IHttpRequestFeature.PathBase
        {
            get => pathBase ?? string.Empty;
            set => pathBase = value;
        }

        /// <inheritdoc />
        string IHttpRequestFeature.Path
        {
            get => path ?? string.Empty;
            set => path = value;
        }

        /// <inheritdoc />
        string IHttpRequestFeature.QueryString
        {
            get => queryString ?? string.Empty;
            set => queryString = value;
        }

        /// <inheritdoc />
        string IHttpRequestFeature.RawTarget { get; set; }

        /// <inheritdoc />
        IHeaderDictionary IHttpRequestFeature.Headers
        {
            get; set;
        }

        /// <inheritdoc />
        Stream IHttpRequestFeature.Body
        {
            get; set;
        }

        #endregion

        #region IHttpResponseFeature Implementation

        /// <inheritdoc />
        int IHttpResponseFeature.StatusCode
        {
            get; set;
        }

        /// <inheritdoc />
        string IHttpResponseFeature.ReasonPhrase
        {
            get; set;
        }

        /// <inheritdoc />
        IHeaderDictionary IHttpResponseFeature.Headers
        {
            get; set;
        }

        /// <inheritdoc />
        Stream IHttpResponseFeature.Body
        {
            get; set;
        }

        /// <inheritdoc />
        bool IHttpResponseFeature.HasStarted
        {
            get
            {
                if (this.currentHttpResponseFeature.Body == null) return false;
                return this.currentHttpResponseFeature.Body.Length > 0;
            }
        }

        /// <inheritdoc />
        void IHttpResponseFeature.OnStarting(Func<object, Task> callback, object state)
        {
            lock (onStartingSync)
            {
                OnStartingActions.Add(new KeyValuePair<Func<object, Task>, object>(callback, state));
            }
        }

        /// <inheritdoc />
        void IHttpResponseFeature.OnCompleted(Func<object, Task> callback, object state)
        {
            lock (onCompletedSync)
            {
                OnCompletedActions.Add(new KeyValuePair<Func<object, Task>, object>(callback, state));
            }
        }

        #endregion

        #region IHttpConnectionFeature Implementation

        /// <inheritdoc />
        IPAddress IHttpConnectionFeature.RemoteIpAddress { get; set; }

        /// <inheritdoc />
        IPAddress IHttpConnectionFeature.LocalIpAddress { get; set; }

        /// <inheritdoc />
        int IHttpConnectionFeature.RemotePort { get; set; }

        /// <inheritdoc />
        int IHttpConnectionFeature.LocalPort { get; set; }

        /// <inheritdoc />
        string IHttpConnectionFeature.ConnectionId { get; set; }

        #endregion

        /// <inheritdoc />
        public void Dispose()
        {
            if (this.disposed) return;

            this.disposed = true;

            try
            {
                this.currentHttpRequestFeature.Body?.Dispose();
            }
            catch
            {
                // ignored
            }

            try
            {
                this.currentHttpResponseFeature.Body?.Dispose();
            }
            catch
            {
                // ignored
            }

            if (this.otherFeatures == null) return;

            foreach (var feature in this.otherFeatures)
            {
                if (!(feature.Value is IDisposable disposable)) continue;

                try
                {
                    disposable.Dispose();
                }
                catch
                {
                    // ignored
                }
            }
        }
    }
}

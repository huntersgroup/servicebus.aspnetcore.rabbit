﻿using ServiceBus.Core.Descriptors;

namespace ServiceBus.AspNetCore.Subscribers.Descriptors
{
    /// <summary>
    /// Represents basic configuration for HostSubscribers
    /// </summary>
    public class RestHostDescriptor : RestBrokerDescriptor
    {
        /// <summary>
        /// Gets or sets the prefetch value used to subscribers.
        /// <para>
        /// The default value is 1.
        /// </para>
        /// </summary>
        public ushort PrefetchCount { get; set; } = 1;

        /// <summary>
        /// Gets or sets the value use to indicate if subscribers don't need to confirm explicitly incoming messages.
        /// <para>
        /// In the most cases, this default value (false) is advisable, only for certain scenarios when clients would prefer not to have any guarantees if messages were processed correctly. 
        /// </para>
        /// </summary>
        public bool AutoAck { get; set; } = false;
    }
}

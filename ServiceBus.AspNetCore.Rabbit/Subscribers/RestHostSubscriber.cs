﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceBus.AspNetCore.Extensions;
using ServiceBus.AspNetCore.Features;
using ServiceBus.AspNetCore.Hosting;
using ServiceBus.AspNetCore.Subscribers.Descriptors;
using ServiceBus.Core.Clients;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.Http.Response;
using ServiceBus.Core.IO;

namespace ServiceBus.AspNetCore.Subscribers
{
    /// <summary>
    /// 
    /// </summary>
    public class RestHostSubscriber<TScope> : BrokerClient
        where TScope : ContextScope
    {
        private readonly object lockObj = new object();
        private readonly RestHostDescriptor descriptor;
        private readonly IHttpRestMessageSerializer restMessageSerializer;
        private readonly IMemoryStreamResolver streamResolver;
        private readonly IHttpApplication<TScope> application;
        private readonly ILogger logger;

        private bool isStarted;
        private bool disposed;

        private string exchangeName;
        private EventingBasicConsumer consumer;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="descriptor"></param>
        /// <param name="restMessageSerializer"></param>
        /// <param name="streamResolver"></param>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public RestHostSubscriber(RestHostDescriptor descriptor,
            IHttpRestMessageSerializer restMessageSerializer,
            IMemoryStreamResolver streamResolver,
            IHttpApplication<TScope> application,
            ILogger logger)
            : base(descriptor)
        {
            this.descriptor = descriptor;
            this.restMessageSerializer = restMessageSerializer;
            this.streamResolver = streamResolver;
            this.application = application;
            this.logger = logger;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            lock (this.lockObj)
            {
                if (this.isStarted)
                {
                    throw new InvalidOperationException("This Subscriber instance was started before !");
                }

                this.isStarted = true;
            }
            
            var exchangeDescriptor = new ExchangeDescriptor { Name = this.descriptor.ServiceName, Durable = this.descriptor.Durable, AutoDelete = false };
            var workingQueueDescriptor = new QueueDescriptor { Name = this.descriptor.ServiceName, Durable = this.descriptor.Durable, AutoDelete = false };
            var workingQueueBinderDescriptor = new BinderDescriptor();

            this.exchangeName = exchangeDescriptor.GetChannelName();
            
            // prepares working queue, exchange and working binder for queue flow.
            this.Channel.BuildExchange(exchangeDescriptor)
                .BuildQueue(workingQueueDescriptor)
                .BuildBinder(exchangeDescriptor, workingQueueBinderDescriptor, workingQueueDescriptor);

            this.consumer = new EventingBasicConsumer(this.Channel);

            this.consumer.Received += async (sender, args) =>
            {
                try
                {
                    await this.ConsumerOnReceived(args);
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, "Something was occurred when host subscriber tried to consume the incoming message");
                }
            };

            var queueName = workingQueueDescriptor.GetChannelName();

            this.Channel.BasicQos(0, descriptor.PrefetchCount, false);
            this.Channel.BasicConsume(queue: queueName, autoAck: descriptor.AutoAck, consumer: this.consumer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            lock (this.lockObj)
            {
                if (this.disposed) return;

                this.disposed = true;
                this.Channel.Cancel(this.consumer);
            }

            base.Dispose(disposing);
        }

        private async Task ConsumerOnReceived(BasicDeliverEventArgs e)
        {
            var body = e.Body;
            var props = e.BasicProperties;
            var replyProps = this.Channel.CreateBasicProperties();
            replyProps.CorrelationId = props.CorrelationId;

            var appContextCreated = false;
            
            HttpRestRequest request;

            try
            {
                request = this.restMessageSerializer.DeserializeRequest(body);
            }
            catch (Exception ex)
            {
                this.logger.LogCritical($"An exception was occurred when the incoming received message, message: {ex.Message}");
                this.logger.LogCritical($"Stacktrace: {ex.StackTrace}");

                this.Channel.BasicReject(e.DeliveryTag, false);
                
                return;
            }

            var context = default(TScope);

            var feature = request.AsFeatureMessage(this.streamResolver);

            try
            {
                context = this.application.CreateContext(feature);
                appContextCreated = true;

                await this.application.ProcessRequestAsync(context)
                    .ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                this.ReportApplicationError(feature, ex);
            }
            finally
            {
                if (!feature.HasApplicationException)
                {
                    await this.FireOnResponseStarting(feature)
                        .ConfigureAwait(false);
                }
            }

            if (feature.HasApplicationException)
            {
                feature.Dispose();
                feature = HttpStatusCode.InternalServerError.AsFeatureMessage(null, this.streamResolver);
            }

            // send response!
            HttpRestResponse response;

            try
            {
                response = feature.AsHttpRestResponse();
            }
            catch (Exception ex)
            {
                response = ex.AsFeatureMessage(this.streamResolver, this.restMessageSerializer.Encoder).AsHttpRestResponse();
            }

            // callback response
            try
            {
                if (!string.IsNullOrWhiteSpace(props.ReplyTo))
                {
                    var responseBytes = this.restMessageSerializer.SerializeResponse(response);

                    this.Channel.BasicPublish(exchange: this.exchangeName, routingKey: props.ReplyTo, basicProperties: replyProps, body: responseBytes);
                }

                this.Channel.BasicAck(e.DeliveryTag, false);
            }
            catch (Exception ex)
            {
                this.logger.LogError($"Something was happened when the response was sent into client publisher, message: {ex.Message}");
            }

            if (appContextCreated)
            {
                await FireOnResponseCompleted(feature)
                    .ConfigureAwait(false);

                this.application.DisposeContext(context, feature.ApplicationException);
            }

            feature?.Dispose();

            await Task.CompletedTask;
        }

        private void ReportApplicationError(ServiceFeatureMessage msg, Exception ex)
        {
            msg.ApplicationException = ex;
            this.logger.LogError($"Subscription error: {ex.Message}");
        }

        private async Task FireOnResponseStarting(ServiceFeatureMessage msg)
        {
            var onStarting = msg.OnStartingActions;
            if (onStarting != null)
            {
                try
                {
                    foreach (var entry in onStarting)
                    {
                        await entry.Key.Invoke(entry.Value);
                    }
                }
                catch (Exception ex)
                {
                    ReportApplicationError(msg, ex);
                }
            }
        }

        private async Task FireOnResponseCompleted(ServiceFeatureMessage msg)
        {
            var onCompleted = msg.OnStartingActions;
            if (onCompleted != null)
            {
                foreach (var entry in onCompleted)
                {
                    try
                    {
                        await entry.Key.Invoke(entry.Value);
                    }
                    catch (Exception ex)
                    {
                        ReportApplicationError(msg, ex);
                    }
                }
            }
        }
    }
}

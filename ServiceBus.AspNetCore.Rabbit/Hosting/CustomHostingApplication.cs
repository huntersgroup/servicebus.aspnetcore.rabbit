﻿using System;
////using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Logging;

namespace ServiceBus.AspNetCore.Hosting
{
    /// <summary>
    /// Represents a custom Http application host.
    /// </summary>
    public class CustomHostingApplication : IHttpApplication<ContextScope>
    {
        private const string NullRequestTarget = "request: [null]";
        private const string NullResponseTarget = "response: [null]";
        private readonly RequestDelegate application;
        private readonly IHttpContextFactory httpContextFactory;
        private readonly ILogger logger;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        /// <param name="httpContextFactory"></param>
        public CustomHostingApplication(
            RequestDelegate application,
            ILogger logger,
            //DiagnosticListener diagnosticSource,
            IHttpContextFactory httpContextFactory)
        {
            this.application = application;
            this.logger = logger;
            this.httpContextFactory = httpContextFactory;
        }

        ///<inheritdoc />
        public ContextScope CreateContext(IFeatureCollection contextFeatures)
        {
            var context = new ContextScope();
            var httpContext = httpContextFactory.Create(contextFeatures);

            try
            {
                this.NormalizeHttpContext(contextFeatures, httpContext);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, $"id: {context.Id}, message: {ex.Message}");
            }

            context.HttpContext = httpContext;
            
            return context;
        }

        ///<inheritdoc />
        public Task ProcessRequestAsync(ContextScope context)
        {
            context.StartTime ??= DateTime.UtcNow;
            
            this.logger.LogInformation($"id: {context.Id}, startTime: {context.StartTime:u}, {this.FormatRequest(context.HttpContext.Request)}");
            return application(context.HttpContext);
        }

        ///<inheritdoc />
        public void DisposeContext(ContextScope context, Exception exception)
        {
            context.EndTime ??= DateTime.UtcNow;
            var httpContext = context.HttpContext;

            this.logger.LogInformation($"id: {context.Id}, endTime: {context.EndTime:u}, {this.FormatResponse(context.HttpContext.Response)}");
            httpContextFactory.Dispose(httpContext);
        }

        private void NormalizeHttpContext(IFeatureCollection contextFeatures, HttpContext ctx)
        {
            var rsp = contextFeatures.Get<IHttpResponseFeature>();

            // It's necessary for netcore 3.1
            // new operator ??= fails always.. don't change it !!!
            ctx.Response.Body = rsp.Body;
        }

        private string FormatRequest(HttpRequest request)
        {
            return request == null ? NullRequestTarget : $"request: [method: {request.Method}, route: {request.Path}]";
        }

        private string FormatResponse(HttpResponse response)
        {
            return response == null ? NullResponseTarget : $"response: [statusCode: {response.StatusCode}, contentLength: {response.ContentLength}]";
        }
    }
}

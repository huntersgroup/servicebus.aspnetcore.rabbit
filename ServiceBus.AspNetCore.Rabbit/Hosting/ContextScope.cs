﻿using System;
using Microsoft.AspNetCore.Http;

namespace ServiceBus.AspNetCore.Hosting
{
    /// <summary>
    /// 
    /// </summary>
    public class ContextScope
    {
        /// <summary>
        /// 
        /// </summary>
        public ContextScope()
        {
            this.Id = Guid.NewGuid().ToString("D");
        }

        /// <summary>
        /// 
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// 
        /// </summary>
        public HttpContext HttpContext { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? EndTime { get; set; }

        ////public long StartTimestamp { get; set; }
        ////public bool EventLogEnabled { get; set; }
        ////public Activity Activity { get; set; }
    }
}

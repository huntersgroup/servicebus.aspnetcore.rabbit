﻿using System;
using System.Linq;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Primitives;
using ServiceBus.AspNetCore.Features;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Http;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.Http.Response;
using ServiceBus.Core.IO;

namespace ServiceBus.AspNetCore.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class MessageTranslators
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="streamResolver"></param>
        /// <returns></returns>
        public static ServiceFeatureMessage AsFeatureMessage(this HttpRestRequest request, IMemoryStreamResolver streamResolver)
        {
            var feature = new ServiceFeatureMessage();
            IHttpRequestFeature req = feature;
            IHttpResponseFeature res = feature;

            var uri = request.Resource.BuildUri(null);

            req.Path = uri.AbsolutePath.UnEscapeUriString();
            req.RawTarget = uri.AbsolutePath;
            req.Protocol = $"{HttpConstants.PrefixHttpVersion}{request.Version}";
            req.QueryString = uri.Query;
            req.Method = request.Method.Translate();

            // request headers
            var reqHeaders = new HeaderDictionary();

            var headers = request.Headers
                .Where(pair => !pair.Key.Equals(HttpHeaders.ContentLength, StringComparison.OrdinalIgnoreCase))
                .ToList();

            foreach (var header in headers)
            {
                reqHeaders.Add(header.Key, header.Value.ToArray());
            }

            if (request.Content != null && request.Content.Any())
            {
                req.Body = streamResolver.Resolve(request.Content);

                reqHeaders.ContentLength = request.Content.Length;
            }

            req.Headers = reqHeaders;

            // create response
            res.Body = streamResolver.Resolve();
            res.StatusCode = 200;

            res.Headers = new HeaderDictionary
            {
                { nameof(HttpHeaders.Server), new StringValues(HttpHeaders.Server) }
            };

            return feature;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="streamResolver"></param>
        /// <param name="encoder"></param>
        /// <returns></returns>
        public static ServiceFeatureMessage AsFeatureMessage(this Exception ex, IMemoryStreamResolver streamResolver, Encoding encoder)
        {
            var sb = new StringBuilder();
            sb.Append("Exception: \r\n\r\n");
            sb.Append(ex.Message);
            sb.Append("\r\n\r\nStackTrace: \r\n\r\n");
            sb.Append(ex.StackTrace);

            if (ex.InnerException != null)
            {
                sb.Append("Inner Exception: \r\n\r\n");
                sb.Append(ex.InnerException.Message);
                sb.Append("\r\n\r\nStackTrace: \r\n\r\n");
                sb.Append(ex.InnerException.StackTrace);
            }

            return HttpStatusCode.InternalServerError.AsFeatureMessage("An unexpected exception was thrown.", streamResolver, encoder.GetBytes(sb.ToString()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="status"></param>
        /// <param name="reasonPhrase"></param>
        /// <param name="streamResolver"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public static ServiceFeatureMessage AsFeatureMessage(this HttpStatusCode status, string reasonPhrase, IMemoryStreamResolver streamResolver, byte[] body = null)
        {
            var msg = new ServiceFeatureMessage();
            IHttpResponseFeature resp = msg;

            resp.StatusCode = (int)status;
            resp.ReasonPhrase = reasonPhrase;

            if (body != null)
            {
                resp.Body = streamResolver.Resolve(body);
            }

            return msg;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="feature"></param>
        /// <returns></returns>
        public static HttpRestResponse AsHttpRestResponse(this ServiceFeatureMessage feature)
        {
            var response = new HttpRestResponse();
            IHttpResponseFeature res = feature;

            if (res.Headers != null)
            {
                foreach (var hdr in res.Headers)
                {
                    response.Headers.Add(hdr.Key, hdr.Value.ToList());
                }
            }

            response.Version = HttpConstants.DefaultHttpVersion;
            response.StatusCode = res.StatusCode;

            response.StatusDescription = string.IsNullOrEmpty(res.ReasonPhrase) ? response.StatusCode.ToReasonPhrase() : res.ReasonPhrase;

            response.Content = res.Body.ToArray();

            return response;
        }
    }
}

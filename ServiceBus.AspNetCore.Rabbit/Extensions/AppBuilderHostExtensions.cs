﻿using System;
//using System.Diagnostics;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ServiceBus.AspNetCore.Hosting;
using ServiceBus.AspNetCore.Subscribers;
using ServiceBus.AspNetCore.Subscribers.Descriptors;
using ServiceBus.Core.IO;

namespace ServiceBus.AspNetCore.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class AppBuilderHostExtensions
    {
        /// <summary>
        /// Runs a custom application host which holds all incoming rabbit messages, translating into http messages in order to forward to API controllers.
        /// <para>
        /// In order to use this method correctly, be sure to register these dependencies on current service provider: <see cref="RestHostDescriptor"/>, <see cref="IHttpRestMessageSerializer"/> and <see cref="IMemoryStreamResolver"/>
        /// </para>
        /// </summary>
        /// <param name="app"></param>
        public static void RunSubscriberHost(this IApplicationBuilder app)
        {
            app.RunSubscriberHost(app.ApplicationServices);
        }

        /// <summary>
        /// Runs a custom application host which holds all incoming rabbit messages, translating into http messages in order to forward to API controllers.
        /// <para>
        /// The given service provider must contain all these registrations: <see cref="RestHostDescriptor"/>, <see cref="IHttpRestMessageSerializer"/> and <see cref="IMemoryStreamResolver"/>
        /// </para>
        /// </summary>
        /// <param name="app"></param>
        /// <param name="serviceProvider"></param>
        public static void RunSubscriberHost(this IApplicationBuilder app, IServiceProvider serviceProvider)
        {
            var descriptor = serviceProvider.GetRequiredService<RestHostDescriptor>();
            var restMessageSerializer = serviceProvider.GetRequiredService<IHttpRestMessageSerializer>();
            var streamResolver = serviceProvider.GetRequiredService<IMemoryStreamResolver>();

            app.RunSubscriberHost(descriptor, restMessageSerializer, streamResolver);
        }

        /// <summary>
        /// Runs a custom application host which holds all incoming rabbit messages, translating into http messages in order to forward to API controllers.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="descriptor"></param>
        /// <param name="restMessageSerializer"></param>
        /// <param name="streamResolver"></param>
        public static void RunSubscriberHost(this IApplicationBuilder app, RestHostDescriptor descriptor, IHttpRestMessageSerializer restMessageSerializer, IMemoryStreamResolver streamResolver)
        {
            var appFunc = app.Build();

            var loggerFactory = app.ApplicationServices.GetRequiredService<ILoggerFactory>();
            ////var diagnosticListener = app.ApplicationServices.GetRequiredService<DiagnosticListener>();
            var httpContextFactory = app.ApplicationServices.GetRequiredService<IHttpContextFactory>();
            var appLifeTime = app.ApplicationServices.GetRequiredService<IApplicationLifetime>();

            var application = new CustomHostingApplication(appFunc, loggerFactory.CreateLogger(typeof(CustomHostingApplication)), httpContextFactory);

            var host = new RestHostSubscriber<ContextScope>(descriptor, restMessageSerializer, streamResolver, application, loggerFactory.CreateLogger<RestHostSubscriber<ContextScope>>());

            appLifeTime.ApplicationStopping.Register(() =>
            {
                IDisposable disposable = host;
                disposable.Dispose();
            });

            host.Start();
        }
    }
}

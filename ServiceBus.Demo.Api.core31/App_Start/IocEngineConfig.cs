﻿using System;
using System.Reflection;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IO;
using ServiceBus.Core.IO;
using ServiceResolver.Ioc;
using ServiceResolver.Ioc.SimpleInjector;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace ServiceBus.Demo.Api
{
    internal static class IocEngineConfig
    {
        /// <summary>
        /// Register all business services into new service provider.
        /// <para>
        /// Calling this method, It enables the resolution of all injecting business / other services using a custom Controller factory.
        /// </para>
        /// </summary>
        /// <param name="services"></param>
        /// <returns>Returns a custom service provider with all business service resolutions.</returns>
        internal static IServiceProvider RegisterServices(this IServiceCollection services)
        {
            var serviceRegisterProvider = new ServiceRegisterProvider(new ServiceRegisterDescriptor
            {
                ScopeFactory = AsyncScopedLifestyle.BeginScope,
                Initializer = container => ContainerInit(container, services)
            });

            serviceRegisterProvider.RegisterServices();

            // register modules presents in this assembly, or in another referenced to this application.
            serviceRegisterProvider.RegisterModulesFrom(Assembly.GetExecutingAssembly());
            
            // verifies the current ioc container if everything is ok, if there are all services / dependencies registered.
            serviceRegisterProvider.Verify();

            return serviceRegisterProvider;
        }

        /// <summary>
        /// Register all services needed to this <see cref="IServiceRegisterProvider"/> instance.
        /// </summary>
        /// <param name="serviceRegisterProvider"></param>
        private static void RegisterServices(this IServiceRegisterProvider serviceRegisterProvider)
        {
            serviceRegisterProvider.Register(() => new RecyclableMemoryStreamManager(), LifetimeScope.Singleton);

            serviceRegisterProvider.Register<IHttpRestMessageSerializer, HttpRestMessageSerializer>()
                //.Register<IMemoryStreamResolver>(() => new FuncMemoryStreamResolver(() => new MemoryStream(), bytes => new MemoryStream(bytes)), LifetimeScope.Singleton)
                .Register<IMemoryStreamResolver>(serviceProvider =>
                {
                    var streamManager = serviceProvider.GetService<RecyclableMemoryStreamManager>();
                    var ms = new FuncMemoryStreamResolver(() => streamManager.GetStream(), bytes => streamManager.GetStream(bytes));

                    return ms;
                })
                .Register(() => Encoding.UTF8);
        }

        /// <summary>
        /// An initializer for the given container
        /// </summary>
        /// <param name="container"></param>
        /// <param name="services"></param>
        private static void ContainerInit(Container container, IServiceCollection services)
        {
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            services.AddSimpleInjector(container, options =>
            {
                options.AddAspNetCore()
                    .AddControllerActivation();

                // you can enable other features for aspnet.core
            });
        }
    }
}

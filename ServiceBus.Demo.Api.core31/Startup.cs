using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using ServiceBus.AspNetCore.Extensions;
using ServiceBus.Core.IO;
using ServiceBus.Demo.Api.Config;

namespace ServiceBus.Demo.Api
{
    /// <summary>
    /// 
    /// </summary>
    internal class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="environment"></param>
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            var assembly = Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly();
            var assemblyName = assembly.GetName();

            //Version of system
            ApplicationName = assemblyName.Name;
            ApplicationVersion = $"v{assemblyName.Version.Major}" +
                                 $".{assemblyName.Version.Minor}" +
                                 $".{assemblyName.Version.Build}";

            this.Configuration = configuration;
            this.Environment = environment;

            this.AppSettings = configuration.Get<AppSettings>();
        }

        /// <summary>
        /// Application name
        /// </summary>
        public string ApplicationName { get; }

        /// <summary>
        /// Application version
        /// </summary>
        public string ApplicationVersion { get; }

        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// 
        /// </summary>
        public IWebHostEnvironment Environment { get; }

        /// <summary>
        /// 
        /// </summary>
        public IServiceProvider ServiceProvider { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public AppSettings AppSettings { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            // It's better to register all service for this application pipeline
            services.AddMvc();

            this.ServiceProvider = services.RegisterServices();

            services.ConfigSwagger(new OpenApiInfo { Title = this.ApplicationName, Version = this.ApplicationVersion });
        }

        /// <summary>
        /// Executed in order to configure the application pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="appLifetime"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime appLifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            // set this BEFORE MapControllers and before UseAuthorization
            app.UseRouting();

            //Enable CORS
            //app.UseCors("CorsPolicy");

            app.UseForwardedHeaders();

            //Enable authorization checks, always after UseRouting
            app.UseAuthorization();

            //Enable swagger 
            app.UseSwagger(options =>
            {
                options.PreSerializeFilters.Add((document, request) =>
                {
                    document.Servers = new List<OpenApiServer>
                    {
                        new OpenApiServer{ Url = string.Empty }
                    };
                });
            });

            //Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("v1/swagger.json", $"{ApplicationName} {ApplicationVersion}");
            });

            //app.Use(async (context, next) =>
            //{
            //    //var cultureQuery = context.Request.Query["culture"];
            //    var features = context.Features;

            //    if (features is IHttpRequestFeature req)
            //    {
            //        Console.WriteLine(req);
            //    }

            //    // Call the next delegate/middleware in the pipeline
            //    await next();
            //});

            // this must be the last setting!
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            //HostingApplication aa;
            //DefaultHttpContextFactory aa;
            //HttpContextFactory bb;

            app.RunSubscriberHost(this.AppSettings.BrokerDescriptor, this.ServiceProvider.GetService<IHttpRestMessageSerializer>(), this.ServiceProvider.GetService<IMemoryStreamResolver>());

            appLifetime.ApplicationStopping.Register(OnShutdown);
        }

        private void OnShutdown()
        {
            // Application is stopping !!!
            (this.ServiceProvider as IDisposable)?.Dispose();
        }
    }
}

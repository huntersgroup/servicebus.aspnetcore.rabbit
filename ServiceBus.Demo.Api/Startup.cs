﻿using System;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using ServiceBus.AspNetCore.Extensions;
using ServiceBus.Core.IO;
using ServiceBus.Demo.Api.Config;

namespace ServiceBus.Demo.Api
{
    /// <summary>
    /// 
    /// </summary>
    internal class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="environment"></param>
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            var assembly = Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly();
            var assemblyName = assembly.GetName();

            //Version of system
            ApplicationName = assemblyName.Name;
            ApplicationVersion = $"v{assemblyName.Version.Major}" +
                                 $".{assemblyName.Version.Minor}" +
                                 $".{assemblyName.Version.Build}";

            this.Configuration = configuration;
            this.Environment = environment;

            this.AppSettings = configuration.Get<AppSettings>();
        }

        /// <summary>
        /// Application name
        /// </summary>
        public string ApplicationName { get; }

        /// <summary>
        /// Application version
        /// </summary>
        public string ApplicationVersion { get; }

        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// 
        /// </summary>
        public IHostingEnvironment Environment { get; }

        /// <summary>
        /// 
        /// </summary>
        public IServiceProvider ServiceProvider { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public AppSettings AppSettings { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            // It's better to register all service for this application pipeline
            services.AddMvc();
            
            this.ServiceProvider = services.RegisterServices();

            services.ConfigSwagger(new OpenApiInfo { Title = this.ApplicationName, Version = this.ApplicationVersion });
        }

        /// <summary>
        /// Executed in order to configure the application pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="appLifetime"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime appLifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseForwardedHeaders();

            //Enable swagger 
            app.UseSwagger();

            //Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("v1/swagger.json", $"{ApplicationName} {ApplicationVersion}");
            });

            //app.Use(async (context, next) =>
            //{
            //    //var cultureQuery = context.Request.Query["culture"];
            //    var features = context.Features;

            //    if (features is IHttpRequestFeature req)
            //    {
            //        Console.WriteLine(req);
            //    }

            //    // Call the next delegate/middleware in the pipeline
            //    await next();
            //});
            
            app.UseMvc();

            app.RunSubscriberHost(this.AppSettings.BrokerDescriptor, this.ServiceProvider.GetService<IHttpRestMessageSerializer>(), this.ServiceProvider.GetService<IMemoryStreamResolver>());
            
            appLifetime.ApplicationStopping.Register(OnShutdown);
        }

        private void OnShutdown()
        {
            // Application is stopping !!!
            (this.ServiceProvider as IDisposable)?.Dispose();
        }
    }
}

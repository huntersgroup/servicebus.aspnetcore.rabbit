﻿using System.Collections.Generic;
using System.Text;
using ServiceResolver.Ioc;
using ServiceResolver.Ioc.Modules;
using ServiceResolver.Ioc.Registers;

namespace ServiceBus.Demo.Api.Modules
{
    /// <summary>
    /// 
    /// </summary>
    public class DemoServiceModule : IServiceModule
    {
        /// <inheritdoc />
        public void Initialize(IServiceRegister serviceRegisterProvider)
        {
            serviceRegisterProvider.Register(() => new StringBuilder("ciao a tutti"));
            serviceRegisterProvider.Register<IList<Models.Demo>>(() => new List<Models.Demo>(), LifetimeScope.Singleton);
        }
    }
}

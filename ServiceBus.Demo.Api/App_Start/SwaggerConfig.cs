﻿using System.IO;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;

namespace ServiceBus.Demo.Api
{
    /// <summary>
    /// 
    /// </summary>
    internal static class SwaggerConfig
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        /// <param name="info"></param>
        internal static void ConfigSwagger(this IServiceCollection services, OpenApiInfo info)
        {
            services.AddSwaggerGen(swaggerOptions =>
            {
                //Head information
                swaggerOptions.SwaggerDoc("v1", info);

                //Add comments for controllers
                var file = $"{typeof(Startup).Assembly.GetName().Name}.xml";
                var xmlPath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, file);
                swaggerOptions.IncludeXmlComments(xmlPath);
            });
        }
    }
}
